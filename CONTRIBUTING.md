# Contributing

Thanks for your interest in contributing to the `@gitlab/web-ide` project!

You can start contributing by following the instructions in the
[Development Environment Setup](./docs/contributing/development-environment-setup.md). Then, be sure to familiarize yourself with the
[Style Guide](./docs/contributing/style-guide.md).
