---
stage: Create
group: remote development
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# Developer guides

This directory contains development guides for the GitLab Web IDE project.
You can find the GitLab Web IDE's user documentation in the
[GitLab user documentation](https://docs.gitlab.com/ee/user/project/web_ide/) site.

## Contribution guidelines

These guides focus on general topics around contributing to the GitLab Web IDE project:

- [Development environment setup](./contributing/development-environment-setup.md) - Start here
- [Style Guide](./contributing/style-guide.md)
- [Developer FAQ & Troubleshooting](./contributing/faq-and-troubleshooting.md)

### Maintaining

These guides focus on workflows pertaining to maintaining the GitLab Web IDE project:

- [Maintainer Responsibility](./contributing/maintainer-responsibility.md)
- [Security Releases](./contributing/security-releases.md)

## Technical guides

These guides dive deep into more specific technical topics of the Web IDE:

- [Architecture of packages](./development/architecture-packages.md)
- [Deployment](./development/deployment.md)
- [Working with packages](./development/working-with-packages.md)
- [Settings Sync](./development/settings-sync.md)
- [Working with VS Code extensions](./development/vscode-extensions.md)
- [GitLab Workflow Extension](./development/vscode-workflow-extension.md)
- [GitLab VSCode theme](./development/gitlab-vscode-theme.md)
- [Instrumentation](./development/instrumentation.md)
