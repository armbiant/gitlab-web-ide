---
stage: Create
group: remote development
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# GitLab Workflow Extension

The Web IDE includes the
[GitLab Workflow Extension](https://gitlab.com/gitlab-org/gitlab-vscode-extension) as a built-in extension. This documentation provides guidance on how to test this extension in the Web IDE
during development.

## The `gitlab-vscode-extension` submodule

The GitLab Workflow Extension is included as a git submodule in the directory
`gitlab-vscode-extension`. By running `yarn start:example`, the Web IDE's test
server will initialize and fetch the submodule's pinned tag unless the submodule
is already initialized.

If you've initialized `gitlab-vscode-extension` before, you can use the command
`git submodule update` to check out the latest pinned version.

## GitLab Language Server

The GitLab Workflow Extension uses the
[GitLab Language Server](https://gitlab.com/gitlab-org/editor-extensions/gitlab-lsp) (gitlab-lsp) for features
like GitLab Duo code suggestions. In the Web IDE, the gitlab-lsp is disabled by default, and you can enable it
by setting the user configuration `gitlab.featureFlags.languageServerWebIDE` to `true`.

Follow [these instructions](https://gitlab.com/gitlab-org/gitlab-vscode-extension/-/blob/main/docs/developer/language-server.md?ref_type=heads#link-the-language-server-node-module) to
link your language server project to the `gitlab-vscode-extension` git submodule.
