import type { Disposable } from '@gitlab/web-ide-types';

interface BaseChannel<Message, MessageKey> extends Disposable {
  postMessage(message: Message): void;
  waitForMessage<T extends Message = Message>(messageKey: MessageKey): Promise<T>;
  addEventListener<T extends Message = Message>(
    messageKey: MessageKey,
    callback: (message: T) => void,
  ): Disposable;
}

export interface AuthenticationTokenRequestMessage {
  key: 'authentication-token-request';
}

export interface AuthenticationTokenResponseMessage {
  key: 'authentication-token-response';
  params: {
    token: string;
  };
}

export interface AuthenticationTokenChangedMessage {
  key: 'authentication-token-changed';
}

export type PortChannelMessage =
  | AuthenticationTokenRequestMessage
  | AuthenticationTokenResponseMessage
  | AuthenticationTokenChangedMessage;

export type PortChannelMessageKey = PortChannelMessage['key'];

export interface PortChannel extends BaseChannel<PortChannelMessage, PortChannelMessageKey> {
  readonly messagePort: MessagePort;
  start: () => void;
}

export type PortName = 'auth-port';

export interface PortChannelRequestMessage {
  key: 'port-channel-request';
  params: {
    name: PortName;
  };
}

export interface PortChannelResponseMessage {
  key: 'port-channel-response';
  params: {
    name: PortName;
    port: MessagePort;
  };
}

export interface PortChannelResponseErrorMessage {
  key: 'port-channel-response-error';
  params: {
    name: PortName;
    error: string;
  };
}

export type WindowChannelMessage =
  | PortChannelRequestMessage
  | PortChannelResponseMessage
  | PortChannelResponseErrorMessage;

export type WindowChannelMessageKey = WindowChannelMessage['key'];

export interface CrossWindowChannel
  extends BaseChannel<WindowChannelMessage, WindowChannelMessageKey> {
  /**
   * Requests a PortChannel from the remote origin. The remote
   * origin should've created a `PortChannel` using `createLocalPortChannel(name)`
   * first.
   * @param name
   */
  requestRemotePortChannel(name: PortName): Promise<PortChannel>;
  /**
   * Creates a PortChannel in the local origin. A remote origin
   * can request to communicate with this PortChannel by requesting
   * it using `requestRemotePortChannel(name)`.
   * @param name
   */
  createLocalPortChannel(name: PortName): PortChannel;
}
