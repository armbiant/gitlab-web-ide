import { createConfig, useFakeBroadcastChannel } from '@gitlab/utils-test';
import type { AuthConfig, OAuthConfig } from '@gitlab/web-ide-types';
import { DefaultOAuthClient, setupAutoRefresh } from '@gitlab/oauth-client';
import { getAuthProvider } from './getAuthProvider';

jest.mock('@gitlab/oauth-client/src/OAuthClient');
jest.mock('@gitlab/oauth-client/src/setupAutoRefresh');

const TEST_TOKEN_AUTH: AuthConfig = {
  type: 'token',
  token: 'lorem-ipsum-dolar',
};

const TEST_OAUTH_AUTH: OAuthConfig = {
  type: 'oauth',
  callbackUrl: 'https://example.com/callback_url',
  clientId: '123456',
};

const TEST_OAUTH_TOKEN = 'test-secret-oauth-token';

describe('utils/getAuthProvider', () => {
  useFakeBroadcastChannel();

  const configWithAuth = (auth?: AuthConfig) => ({
    ...createConfig(),
    auth,
  });

  it('returns undefined with undefined auth', () => {
    const provider = getAuthProvider({ config: configWithAuth(undefined) });

    expect(provider).toBeUndefined();
  });

  it('returns token provider with token auth', async () => {
    const provider = getAuthProvider({ config: configWithAuth(TEST_TOKEN_AUTH) });

    const actual = await provider?.getToken();

    expect(provider).not.toBeUndefined();
    expect(actual).toEqual(TEST_TOKEN_AUTH.token);
  });

  it('returns oauth provider with oauth', async () => {
    const provider = getAuthProvider({ config: configWithAuth(TEST_OAUTH_AUTH) });

    const oauthClientInstance = jest.mocked(DefaultOAuthClient).mock.instances[0];

    jest.mocked(oauthClientInstance.getToken).mockResolvedValue({
      accessToken: TEST_OAUTH_TOKEN,
      expiresAt: 0,
    });

    const actual = await provider?.getToken();

    expect(oauthClientInstance.onTokenChange).not.toHaveBeenCalled();
    expect(provider).not.toBeUndefined();
    expect(actual).toBe(TEST_OAUTH_TOKEN);
  });

  it('with ouath, registers onTokenChange listener', () => {
    const spy = jest.fn();

    getAuthProvider({ config: configWithAuth(TEST_OAUTH_AUTH), onTokenChange: spy });

    const oauthClientInstance = jest.mocked(DefaultOAuthClient).mock.instances[0];

    expect(oauthClientInstance.onTokenChange).toHaveBeenCalledTimes(1);
    expect(oauthClientInstance.onTokenChange).toHaveBeenCalledWith(spy);
  });

  it('with oauth, calls setupAutoRefresh', () => {
    getAuthProvider({ config: configWithAuth(TEST_OAUTH_AUTH) });

    const oauthClientInstance = jest.mocked(DefaultOAuthClient).mock.instances[0];

    expect(setupAutoRefresh).toHaveBeenCalledTimes(1);
    expect(setupAutoRefresh).toHaveBeenCalledWith(oauthClientInstance);
  });
});
