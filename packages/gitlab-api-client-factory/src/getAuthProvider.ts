import type { AuthProvider } from '@gitlab/gitlab-api-client';
import { DefaultAuthProvider } from '@gitlab/gitlab-api-client';
import { asOAuthProvider, createOAuthClient, setupAutoRefresh } from '@gitlab/oauth-client';
import type { WebIdeConfig } from '@gitlab/web-ide-types';

interface GetAuthProviderOptions {
  config: WebIdeConfig;
  onTokenChange?: () => void;
}

export const getAuthProvider = ({
  config,
  onTokenChange,
}: GetAuthProviderOptions): AuthProvider | undefined => {
  if (config.auth?.type === 'token') {
    return new DefaultAuthProvider(config.auth.token);
  }
  if (config.auth?.type === 'oauth') {
    const client = createOAuthClient({
      oauthConfig: config.auth,
      gitlabUrl: config.gitlabUrl,
      owner: config.username,
    });

    setupAutoRefresh(client);

    if (onTokenChange) {
      client.onTokenChange(onTokenChange);
    }

    return asOAuthProvider(client);
  }

  return undefined;
};
