import { omit } from 'lodash';

import type { WebIdeConfig, WebIdeExtensionConfig } from '@gitlab/web-ide-types';

export const createConfig = (): WebIdeConfig => ({
  baseUrl: 'https://foo.bar',
  handleError: jest.fn(),
  handleClose: jest.fn(),
  handleTracking: jest.fn(),
  links: {
    feedbackIssue: 'foobar',
    userPreferences: 'user/preferences',
    signIn: 'user/signIn',
  },
  gitlabUrl: 'https://gitlab.com',
  auth: {
    type: 'token',
    token: 'very-secret-token',
  },
  projectPath: 'gitlab-org/gitlab',
  ref: 'main',
  handleContextUpdate: jest.fn(),
});

export const createWebIdeExtensionConfig = (): WebIdeExtensionConfig => ({
  ...omit(createConfig(), ['handleClose', 'handleError', 'handleTracking', 'handleContextUpdate']),
  repoRoot: 'gitlab',
});
