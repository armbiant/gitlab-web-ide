import { useFakeMessageChannel } from '@gitlab/utils-test';
import { MessagePortsController } from './MessagePortsController';

const TEST_CONFIG = {
  baseUrl: 'https://gdk.test:3443/webpack/assets/stuff',
  links: {
    signIn: '',
    feedbackIssue: '',
    userPreferences: '',
  },
};

describe('utils/MessagePortsController', () => {
  useFakeMessageChannel();

  let subject: MessagePortsController;

  describe('default', () => {
    beforeEach(() => {
      subject = new MessagePortsController(TEST_CONFIG);
    });

    it('messagePorts - has messagePort with extension id', () => {
      expect(Array.from(subject.messagePorts.keys())).toEqual(['gitlab.gitlab-web-ide']);
    });

    it('onTokenChange - triggers message port', () => {
      const port = subject.messagePorts.get('gitlab.gitlab-web-ide');

      if (!port) {
        throw new Error('Expected MessagePort to be found');
      }

      const spy = jest.fn();
      port.addEventListener('message', e => spy(e.data));

      subject.onTokenChange();

      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith('webide_auth_change');
    });
  });
});
