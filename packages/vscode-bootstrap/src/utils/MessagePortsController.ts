import { handleMediatorMessages } from '@gitlab/vscode-mediator-commands';
import { WEB_IDE_EXTENSION_ID } from '@gitlab/web-ide-interop';
import type { WebIdeConfig } from '@gitlab/web-ide-types';

type MessagePortsControllerOptions = Pick<WebIdeConfig, 'baseUrl' | 'featureFlags' | 'links'>;

export class MessagePortsController {
  readonly #webIdeChannel: MessageChannel;

  readonly #messagePorts: Map<string, MessagePort>;

  constructor(options: MessagePortsControllerOptions) {
    const webIdeChannel = new MessageChannel();
    const messagePorts = new Map<string, MessagePort>();
    messagePorts.set(WEB_IDE_EXTENSION_ID, webIdeChannel.port2);

    handleMediatorMessages(webIdeChannel.port1, options);

    this.#messagePorts = messagePorts;
    this.#webIdeChannel = webIdeChannel;
  }

  get messagePorts(): ReadonlyMap<string, MessagePort> {
    return this.#messagePorts;
  }

  onTokenChange() {
    this.#webIdeChannel.port1.postMessage('webide_auth_change');
  }
}
