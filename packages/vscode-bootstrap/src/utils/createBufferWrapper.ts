import type { VSBufferWrapper } from '@gitlab/vscode-mediator-commands';
import type { BufferModule } from '../vscode';

export const createBufferWrapper =
  (bufferModule: BufferModule): VSBufferWrapper =>
  (actual: Uint8Array) =>
    bufferModule.VSBuffer.wrap(actual);
