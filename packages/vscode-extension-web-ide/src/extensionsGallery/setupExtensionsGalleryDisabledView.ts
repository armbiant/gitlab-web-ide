import * as vscode from 'vscode';
import type { WebIdeExtensionConfig } from '@gitlab/web-ide-types';
import {
  GO_TO_EXTENSIONS_GALLERY_HELP_COMMAND_ID,
  GO_TO_USER_PREFERENCES_COMMAND_ID,
  GO_TO_ENTERPRISE_GROUP_COMMAND_ID,
  GALLERY_DISABLED_CONTEXT_ID,
  GALLERY_DISABLED_VIEW_OPT_IN,
  GALLERY_DISABLED_VIEW_WITH_DOCS,
  GALLERY_DISABLED_VIEW_DEFAULT,
  GALLERY_DISABLED_VIEW_ENTERPRISE_GROUP,
} from '../constants';
import { NOOP_DISPOSABLE } from '../utils';

interface ViewModel {
  disabledView?: string;

  commands?: {
    id: string;
    url: string;
  }[];
}

const getExtensionsGalleryStatusViewModel = (
  extensionsGallerySettings: WebIdeExtensionConfig['extensionsGallerySettings'],
): ViewModel => {
  if (!extensionsGallerySettings) {
    return {
      disabledView: GALLERY_DISABLED_VIEW_DEFAULT,
    };
  }

  if (extensionsGallerySettings.enabled) {
    return {};
  }

  const commands: { id: string; url: string }[] = [];

  if (extensionsGallerySettings.helpUrl) {
    commands.push({
      id: GO_TO_EXTENSIONS_GALLERY_HELP_COMMAND_ID,
      url: extensionsGallerySettings.helpUrl,
    });
  }

  if (extensionsGallerySettings.reason === 'enterprise_group_disabled') {
    commands.push({
      id: GO_TO_ENTERPRISE_GROUP_COMMAND_ID,
      url: extensionsGallerySettings.enterpriseGroupUrl,
    });

    return {
      disabledView: GALLERY_DISABLED_VIEW_ENTERPRISE_GROUP,
      commands,
    };
  }

  if (
    extensionsGallerySettings.reason === 'opt_in_disabled' ||
    extensionsGallerySettings.reason === 'opt_in_unset'
  ) {
    commands.push({
      id: GO_TO_USER_PREFERENCES_COMMAND_ID,
      url: extensionsGallerySettings.userPreferencesUrl,
    });

    return {
      disabledView: GALLERY_DISABLED_VIEW_OPT_IN,
      commands,
    };
  }

  if (extensionsGallerySettings.helpUrl) {
    return {
      disabledView: GALLERY_DISABLED_VIEW_WITH_DOCS,
      commands,
    };
  }

  return {
    disabledView: GALLERY_DISABLED_VIEW_DEFAULT,
    commands,
  };
};

export const setupExtensionsGalleryDisabledView = async (config: WebIdeExtensionConfig) => {
  const model = getExtensionsGalleryStatusViewModel(config.extensionsGallerySettings);

  if (model.disabledView) {
    await vscode.commands.executeCommand(
      'setContext',
      GALLERY_DISABLED_CONTEXT_ID,
      model.disabledView,
    );
  }

  if (!model.commands?.length) {
    return NOOP_DISPOSABLE;
  }

  const disposables = model.commands.map(({ id, url }) =>
    vscode.commands.registerCommand(id, () => vscode.env.openExternal(vscode.Uri.parse(url))),
  );

  return vscode.Disposable.from(...disposables);
};
