import * as vscode from 'vscode';
import { createWebIdeExtensionConfig } from '@gitlab/utils-test';
import { TEST_PROJECT } from '../../../test-utils';
import {
  showSuccessMessage,
  ITEM_CONTINUE,
  ITEM_CREATE_MR,
  ITEM_GO_TO_PROJECT,
} from './showSuccessMessage';
import { getConfig } from '../../mediator';

jest.mock('../../mediator');

const DEFAULT_ITEMS = [ITEM_GO_TO_PROJECT, ITEM_CONTINUE];
const TEST_BRANCH_NAME = 'root-main-patch-123';

describe('scm/commit/showSuccessMessage', () => {
  it.each`
    projectMixin                              | branchName                     | expectedItems
    ${{}}                                     | ${TEST_BRANCH_NAME}            | ${[ITEM_CREATE_MR, ...DEFAULT_ITEMS]}
    ${{ can_create_merge_request_in: false }} | ${TEST_BRANCH_NAME}            | ${DEFAULT_ITEMS}
    ${{ empty_repo: true }}                   | ${TEST_BRANCH_NAME}            | ${DEFAULT_ITEMS}
    ${{}}                                     | ${TEST_PROJECT.default_branch} | ${DEFAULT_ITEMS}
  `(
    'with projectMixin=$projectMixin and branchName=$branchName, shows items',
    ({ projectMixin, branchName, expectedItems }) => {
      const project = {
        ...TEST_PROJECT,
        ...projectMixin,
      };

      // eslint-disable-next-line @typescript-eslint/no-floating-promises
      showSuccessMessage({ project, branchName });

      expect(vscode.window.showInformationMessage).toHaveBeenCalledWith(
        expect.stringContaining('Success!'),
        ...expectedItems,
      );
    },
  );

  it.each([undefined, ITEM_CONTINUE])('when user selects %s - does nothing', async item => {
    jest.mocked(vscode.window.showInformationMessage).mockResolvedValue(item);

    await showSuccessMessage({ project: TEST_PROJECT, branchName: TEST_BRANCH_NAME });

    expect(vscode.env.openExternal).not.toHaveBeenCalled();
  });

  const NEW_MR_PARAMS = [
    'nav_source=webide',
    `merge_request[source_branch]=${TEST_BRANCH_NAME}`,
    `merge_request[target_branch]=${TEST_PROJECT.default_branch}`,
  ].join('&');

  it.each([
    [ITEM_GO_TO_PROJECT, 'https://gitlab.com/gitlab-org/gitlab'],
    [ITEM_CREATE_MR, `https://gitlab.com/gitlab-org/gitlab/-/merge_requests/new?${NEW_MR_PARAMS}`],
  ])('when user selects %s - opens url %s', async (item, url) => {
    jest.mocked(vscode.window.showInformationMessage).mockResolvedValue(item);
    jest.mocked(getConfig).mockResolvedValue(createWebIdeExtensionConfig());

    await showSuccessMessage({ project: TEST_PROJECT, branchName: TEST_BRANCH_NAME });

    expect(vscode.env.openExternal).toHaveBeenCalledWith(vscode.Uri.parse(url));
  });
});
