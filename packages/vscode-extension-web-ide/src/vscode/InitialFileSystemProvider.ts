/* eslint-disable class-methods-use-this */
import * as vscode from 'vscode';
import { cleanLeadingSeparator } from '@gitlab/utils-path';
import { NOOP_DISPOSABLE } from '../utils';

export const INIT_FILE_STAT = {
  ctime: 0,
  mtime: 0,
  size: -1,
  type: vscode.FileType.Directory,
};

export class InitialFileSystemProvider implements vscode.FileSystemProvider {
  #repoRoot: string;

  constructor(repoRoot: string) {
    this.#repoRoot = repoRoot;
  }

  get onDidChangeFile() {
    return () => NOOP_DISPOSABLE;
  }

  watch(): vscode.Disposable {
    return NOOP_DISPOSABLE;
  }

  stat(uri: vscode.Uri): vscode.FileStat | Thenable<vscode.FileStat> {
    if (cleanLeadingSeparator(uri.path) === cleanLeadingSeparator(this.#repoRoot)) {
      return INIT_FILE_STAT;
    }

    throw vscode.FileSystemError.FileNotFound(uri);
  }

  readDirectory(): [string, vscode.FileType][] | Thenable<[string, vscode.FileType][]> {
    return [];
  }

  readFile(uri: vscode.Uri): Uint8Array | Thenable<Uint8Array> {
    throw vscode.FileSystemError.FileNotFound(uri);
  }

  createDirectory(): void | Thenable<void> {
    throw new Error('Method not implemented.');
  }

  writeFile(): void | Thenable<void> {
    throw new Error('Method not implemented.');
  }

  delete(): void | Thenable<void> {
    throw new Error('Method not implemented.');
  }

  rename(): void | Thenable<void> {
    throw new Error('Method not implemented.');
  }
}
