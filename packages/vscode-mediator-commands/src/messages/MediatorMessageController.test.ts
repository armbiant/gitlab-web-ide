import type { WebIDETrackingMessage, UpdateWebIDEContextMessage } from '@gitlab/web-ide-types';
import { postMessage } from './utils/postMessage';
import { createMockLocation, TEST_CONFIG } from '../../test-utils';
import { createMediatorMessageController } from './MediatorMessageController';
import type { MediatorMessageController } from './MediatorMessageController';
import {
  MESSAGE_OPEN_URI,
  MESSAGE_PREVENT_UNLOAD,
  MESSAGE_READY,
  MESSAGE_SET_HREF,
  MESSAGE_TRACK_EVENT,
  MESSAGE_UPDATE_WEB_IDE_CONTEXT,
} from '../constants';

jest.mock('./utils/postMessage');

const TEST_TRACKING_EVENT_PARMS: WebIDETrackingMessage['params'] = {
  event: {
    name: 'remote-connection-failure',
  },
};

const TEST_UPDATE_WEB_IDE_CONTEXT_PARAMS: UpdateWebIDEContextMessage['params'] = {
  ref: 'test',
  projectPath: 'test-path',
};

describe('messages/MediatorMessageController', () => {
  let subject: MediatorMessageController;

  beforeEach(() => {
    subject = createMediatorMessageController(TEST_CONFIG);
  });

  interface PostMessageTestCase {
    message: keyof MediatorMessageController;
    params: unknown[];
    expected: unknown;
  }
  describe.each`
    message                           | params                                  | expected
    ${MESSAGE_READY}                  | ${[]}                                   | ${{ key: 'ready' }}
    ${MESSAGE_PREVENT_UNLOAD}         | ${[{ shouldPrevent: true }]}            | ${{ key: 'prevent-unload', params: { shouldPrevent: true } }}
    ${MESSAGE_TRACK_EVENT}            | ${[TEST_TRACKING_EVENT_PARMS]}          | ${{ key: 'web-ide-tracking', params: TEST_TRACKING_EVENT_PARMS }}
    ${MESSAGE_UPDATE_WEB_IDE_CONTEXT} | ${[TEST_UPDATE_WEB_IDE_CONTEXT_PARAMS]} | ${{ key: 'update-web-ide-context', params: TEST_UPDATE_WEB_IDE_CONTEXT_PARAMS }}
  `('$message', ({ message, params, expected }: PostMessageTestCase) => {
    beforeEach(() => {
      // note: This cast isn't great, but I can't find a better way to dynamically apply these params
      (subject[message] as (...x: typeof params) => void)(...params);
    });

    it('triggers postMessage', () => {
      expect(postMessage).toHaveBeenCalledTimes(1);
      expect(postMessage).toHaveBeenCalledWith(expected);
    });
  });

  describe(MESSAGE_OPEN_URI, () => {
    beforeEach(() => {
      window.open = jest.fn();
    });

    it.each`
      key                  | uriValue
      ${'feedbackIssue'}   | ${TEST_CONFIG.links.feedbackIssue}
      ${'userPreferences'} | ${TEST_CONFIG.links.userPreferences}
    `('opens $uriValue in new window when uri key is $key', async ({ key, uriValue }) => {
      subject[MESSAGE_OPEN_URI]({ key });

      expect(window.parent.open).toHaveBeenCalledWith(uriValue, '_blank', 'noopener,noreferrer');
    });
  });

  describe(MESSAGE_SET_HREF, () => {
    const origWindowParent = window.parent;

    beforeEach(() => {
      window.parent = { ...window };
      const mockLocation = createMockLocation();
      Object.defineProperty(window.parent, 'location', {
        get() {
          return mockLocation;
        },
      });
    });

    afterEach(() => {
      window.parent = origWindowParent;
    });

    it.each`
      origHref                    | href                                   | expectHref
      ${'http://localhost/'}      | ${'/new/path/place'}                   | ${'http://localhost/new/path/place'}
      ${'http://localhost/-/ide'} | ${'/new/path/place'}                   | ${'http://localhost/new/path/place'}
      ${'http://localhost/-/ide'} | ${'http://example.org/new/path/place'} | ${'http://example.org/new/path/place'}
    `(
      'with origHref=$origHref and href=$href, updates window.parent.location',
      async ({ origHref, href, expectHref }) => {
        window.parent.location.href = origHref;

        await subject[MESSAGE_SET_HREF](href);

        expect(window.parent.location.href).toBe(expectHref);
      },
    );
  });
});
