export const createMockLocation = (): Location => {
  const { ancestorOrigins, hash, host, hostname, href, origin, pathname, port, protocol, search } =
    window.location;

  const location = {
    ancestorOrigins,
    hash,
    host,
    hostname,
    href,
    origin,
    pathname,
    port,
    protocol,
    search,
    assign: jest.fn(),
    reload: jest.fn(),
    replace: jest.fn(),
  };

  return location;
};
