export enum FeatureFlags {
  CrossOriginExtensionHost = 'crossOriginExtensionHost',
  ProjectPushRules = 'projectPushRules',
  LanguageServerWebIDE = 'languageServerWebIDE',
}
