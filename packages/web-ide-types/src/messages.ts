/**
 * This file contains types for messages that are sent to the parent window context
 * through the postMessage API.
 *
 * - Messages must have a unique `key`.
 */
import type { ErrorType } from './error';
import type { TrackingEvent } from './instrumentation';
/**
 * Message sent to tell the parent window that the Web IDE is "ready".
 */
export interface ReadyMessage {
  key: 'ready';
}

/**
 * Message sent to tell the parent window that an error occured.
 */
export interface ErrorMessage {
  key: 'error';
  params: {
    errorType: ErrorType;
  };
}

/**
 * Message sent to tell the parent window that the Web IDE is
 * going to be forcefully closed.
 */
export interface CloseMessage {
  key: 'close';
}

export interface PreventUnloadMessage {
  key: 'prevent-unload';
  params: {
    shouldPrevent: boolean;
  };
}

export interface WebIDETrackingMessage {
  key: 'web-ide-tracking';
  params: {
    event: TrackingEvent;
  };
}

export interface UpdateWebIDEContextMessage {
  key: 'update-web-ide-context';
  params: {
    ref: string;
    projectPath: string;
  };
}

export type Message =
  | ReadyMessage
  | ErrorMessage
  | CloseMessage
  | PreventUnloadMessage
  | WebIDETrackingMessage
  | UpdateWebIDEContextMessage;
