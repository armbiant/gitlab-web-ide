const fs = require('fs');
const path = require('path');

function getPathtoVsCodeDir() {
  return path.resolve(__dirname, '../dist/public/vscode');
}

function renameDirectory(fromPath, toPath) {
  if (!fs.existsSync(fromPath)) {
    console.log('No directory found, skipping...');
    process.exit(0);
  }

  fs.rename(fromPath, toPath, err => {
    if (err) {
      console.error(`Error renaming directory: ${err}`);
      process.exit(1);
    } else {
      console.log(`Successfully renamed "${fromPath}" to "${toPath}"`);
    }
  });
}

module.exports = {
  renameDirectory,
  getPathtoVsCodeDir,
};
