import type { WebIdeConfig } from '@gitlab/web-ide-types';
import { createConfig } from '@gitlab/utils-test';
import { getIframeHtml } from './getIframeHtml';

const defaultConfig = createConfig();

describe('getIframeHtml', () => {
  const collectAttributes = (el: Element): Record<string, string> =>
    Array.from(el.attributes).reduce((acc, attr) => ({ ...acc, [attr.name]: attr.value }), {});

  const collectChildren = (el: Element) =>
    Array.from(el.children)
      .map(child => ({
        tag: child.tagName.toLowerCase(),
        attributes: collectAttributes(child),
        content: child.innerHTML.trim(),
      }))
      .reduce((acc: Record<string, Record<string, string>[]>, { tag, attributes, content }) => {
        acc[tag] = acc[tag] ?? [];

        if (Object.keys(attributes).length) {
          acc[tag].push(attributes);
        }

        if (content) {
          acc[tag].push({ content });
        }

        return acc;
      }, {});

  const createTestSubject = (config: WebIdeConfig) => {
    const html = getIframeHtml(config);

    const parser = new DOMParser();
    const document = parser.parseFromString(html, 'text/html');

    return {
      head: collectChildren(document.head),
      body: collectChildren(document.body),
    };
  };

  type TestParameters = {
    config: WebIdeConfig;
    scriptAttrs: Record<string, string>;
  };

  it.each`
    desc                        | config                                      | scriptAttrs
    ${'client only config'}     | ${defaultConfig}                            | ${{}}
    ${'with nonce'}             | ${{ ...defaultConfig, nonce: 'noncense' }}  | ${{ nonce: 'noncense' }}
    ${'with erroneous baseUrl'} | ${{ ...defaultConfig, baseUrl: '.."123.' }} | ${{}}
  `(
    'with $desc, returns properly escaped head and body elements',
    ({ config, scriptAttrs }: TestParameters) => {
      const subject = createTestSubject(config);

      expect(subject).toStrictEqual({
        head: {
          meta: [
            { charset: 'utf-8' },
            { name: 'viewport', content: expect.any(String) },
            { id: 'gl-config-json', 'data-settings': JSON.stringify(config) },
          ],
          link: [
            {
              'data-name': 'vs/workbench/workbench.web.main',
              rel: 'stylesheet',
              href: `${config.baseUrl}/vscode/out/vs/workbench/workbench.web.main.css`,
            },
          ],
        },
        body: {
          script: [
            {
              src: `${config.baseUrl}/main.js`,
              ...scriptAttrs,
            },
          ],
        },
      });
    },
  );

  it('does not inject fonts if not configured', () => {
    const subject = createTestSubject(defaultConfig);

    const preloadLinks = subject.head.link.filter(link => link.as === 'font');
    expect(preloadLinks).toHaveLength(0);
    expect(subject.head.style).toBeUndefined();
  });

  it('injects the fonts if they are configured', () => {
    const subject = createTestSubject({
      ...defaultConfig,
      editorFont: {
        fallbackFontFamily: '"Deja Vu Sans Mono", monospace',
        fontFaces: [
          {
            family: 'GitLab Mono',
            src: [
              {
                format: 'woff2',
                url: 'http://example.com/fonts/GitLabMono.woff2',
              },
            ],
          },
          {
            family: 'GitLab Mono',
            style: 'italic',
            src: [
              {
                url: 'http://example.com/fonts/GitLabMonoItalic.woff2',
                format: 'woff2',
              },
            ],
          },
          {
            family: "A tester's font",
            src: [
              {
                format: 'opentype',
                url: 'http://example.com/fonts/FooBar.otf',
              },
            ],
            display: 'optional',
            unicodeRange: 'U+0025-00FF',
          },
          {
            family: 'Font with multiple assets',
            src: [
              {
                format: 'opentype',
                url: 'http://example.com/fonts/MultiFont1.otf',
              },
              {
                format: 'woff2',
                url: 'http://example.com/fonts/MultiFont2.woff2',
              },
            ],
          },
        ],
      },
    });

    expect(subject.head.link).toEqual(
      expect.arrayContaining([
        {
          rel: 'preload',
          as: 'font',
          type: 'font/woff2',
          crossorigin: '',
          href: 'http://example.com/fonts/GitLabMono.woff2',
        },
        {
          rel: 'preload',
          as: 'font',
          type: 'font/woff2',
          crossorigin: '',
          href: 'http://example.com/fonts/GitLabMonoItalic.woff2',
        },
        {
          rel: 'preload',
          as: 'font',
          type: 'font/opentype',
          crossorigin: '',
          href: 'http://example.com/fonts/FooBar.otf',
        },
        {
          rel: 'preload',
          as: 'font',
          type: 'font/opentype',
          crossorigin: '',
          href: 'http://example.com/fonts/MultiFont1.otf',
        },
        {
          rel: 'preload',
          as: 'font',
          type: 'font/woff2',
          crossorigin: '',
          href: 'http://example.com/fonts/MultiFont2.woff2',
        },
      ]),
    );

    expect(subject.head.style[0].content).toMatchInlineSnapshot(`
      "@font-face {
      font-family: 'GitLab Mono';
      src: url('http://example.com/fonts/GitLabMono.woff2') format('woff2');
      }
      @font-face {
      font-family: 'GitLab Mono';
      font-style: italic;
      src: url('http://example.com/fonts/GitLabMonoItalic.woff2') format('woff2');
      }
      @font-face {
      font-family: 'A tester%27s font';
      font-display: optional;
      unicode-range: U+0025-00FF;
      src: url('http://example.com/fonts/FooBar.otf') format('opentype');
      }
      @font-face {
      font-family: 'Font with multiple assets';
      src: url('http://example.com/fonts/MultiFont1.otf') format('opentype'),
        url('http://example.com/fonts/MultiFont2.woff2') format('woff2');
      }"
    `);
  });
});
