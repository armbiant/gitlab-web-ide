import { createOAuthClient } from '@gitlab/oauth-client';
import type { WebIdeConfig, Message, WebIde, OAuthCallbackConfig } from '@gitlab/web-ide-types';
import { checkOAuthToken } from './checkOAuthToken';
import { cleanWebIdeExtensions } from './cleanExtensions';
import { getIframeHtml } from './getIframeHtml';
import type { UnloadPreventer } from './unloadPreventer';
import { createUnloadPreventer } from './unloadPreventer';

export const createError = (msg: string) => new Error(`[gitlab-vscode] ${msg}`);

interface HandleMessagesOptions {
  iframe: HTMLIFrameElement;
  config: WebIdeConfig;
  unloadPreventer: UnloadPreventer;
}

const handleMessages = ({ iframe, config, unloadPreventer }: HandleMessagesOptions) => {
  const { contentWindow } = iframe;

  if (!contentWindow) {
    throw createError('Could not find contentWindow for iframe.');
  }

  contentWindow.addEventListener('message', (e: MessageEvent<Message>) => {
    switch (e.data?.key) {
      case 'error':
        config.handleError?.(e.data.params);
        break;
      case 'close':
        config.handleClose?.();
        break;
      case 'prevent-unload':
        unloadPreventer.setShouldPrevent(e.data.params.shouldPrevent);
        break;
      case 'web-ide-tracking':
        config.handleTracking?.(e.data.params.event);
        break;
      case 'update-web-ide-context':
        config.handleContextUpdate?.(e.data.params);
        break;
      default:
        break;
    }
  });
};

const waitForReady = (iframe: HTMLIFrameElement): Promise<void> => {
  const { contentWindow } = iframe;

  if (!contentWindow) {
    throw createError('Could not find contentWindow for iframe.');
  }

  return new Promise<void>(resolve => {
    const listener = (e: MessageEvent<Message>) => {
      if (e.data?.key === 'ready') {
        contentWindow.removeEventListener('message', listener);
        resolve();
      }
    };

    contentWindow.addEventListener('message', listener);
  });
};

const startAnyConfig = (el: Element, config: WebIdeConfig): WebIde => {
  const unloadPreventer = createUnloadPreventer();
  const iframe = document.createElement('iframe');

  Object.assign(iframe.style, {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    width: '100%',
    height: '100%',
    border: 'none',
    margin: 0,
    padding: 0,
  });
  el.appendChild(iframe);

  if (!iframe.contentWindow) {
    throw createError('Could not find contentWindow for iframe.');
  }

  const ready = new Promise<void>((resolve, reject) => {
    // Set "src" to support allow listing https://gitlab.com/gitlab-org/gitlab-web-ide/-/issues/95
    iframe.src = `${config.baseUrl}/assets/placeholder.html`;

    // why: We need to wait for `src` to `load` before writing to document. Otherwise
    //      some weird behavior can happen in certain browsers https://gitlab.com/gitlab-org/gitlab/-/issues/408414
    iframe.addEventListener(
      'load',
      async () => {
        iframe.removeEventListener('error', reject);

        iframe.contentWindow?.document.open();
        iframe.contentWindow?.document.write(getIframeHtml(config));
        iframe.contentWindow?.document.close();

        handleMessages({ iframe, config, unloadPreventer });

        await waitForReady(iframe);

        resolve();
      },
      // why: Prevent forever loop
      { once: true },
    );

    iframe.addEventListener('error', reject, { once: true });
  });

  return {
    dispose() {
      iframe.remove();
      unloadPreventer.dispose();
    },
    ready,
  };
};

export const start = async (el: Element, config: WebIdeConfig): Promise<WebIde> => {
  await checkOAuthToken(config);
  await cleanWebIdeExtensions(config);

  return startAnyConfig(el, config);
};

export const oauthCallback = async (config: OAuthCallbackConfig) => {
  if (config.auth?.type !== 'oauth') {
    throw new Error('Expected config.auth to be OAuth config.');
  }

  const oauthClient = createOAuthClient({
    oauthConfig: config.auth,
    gitlabUrl: config.gitlabUrl,
    owner: config.username,
  });

  return oauthClient.handleCallback();
};
