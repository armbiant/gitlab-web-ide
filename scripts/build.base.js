const { NodeModulesPolyfillPlugin } = require('@esbuild-plugins/node-modules-polyfill');
const esbuild = require('esbuild');

module.exports = options =>
  esbuild
    .build({
      bundle: true,
      plugins: [NodeModulesPolyfillPlugin()],
      loader: {
        '.html': 'text',
      },
      external: ['vscode'],
      sourcemap: 'linked',
      /*
       * Based on gitlab project .browserslistrc file
       * https://gitlab.com/gitlab-org/gitlab/-/blob/master/.browserslistrc
       */
      target: ['chrome103', 'edge103', 'firefox102', 'safari15.6'],
      ...options,
    })
    .catch(() => process.exit(1));
